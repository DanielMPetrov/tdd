package ws2

import org.junit.Assert.*
import org.junit.Test
import java.lang.IllegalArgumentException
import ws2.Role.*

class TeamTests {

    private val playerA = Player(9, "Petrov", FWD)
    private val playerB = Player(8, "Djuricic", DEF)
    private val playerC = Player(11, "Tonwuru", GKP)

    @Test
    fun team_withDefaultConstructor_hasZeroTeamCount() {
        // Arrange
        val sut = Team()

        // Act & Assert
        assertEquals(0, sut.size)
    }

    @Test
    fun addPlayer_incrementsTheTeamCountByOne() {
        // Arrange
        val sut = Team()

        // Act
        sut.addPlayer(playerA)

        // Assert
        assertEquals(1, sut.size)
    }

    @Test(expected = IllegalStateException::class)
    fun addPlayer_whenTeamIsFull_throwsIllegalStateException() {
        // Arrange
        val sut = Team()

        // Act
        for (i in 1..21) {
            sut.addPlayer(Player(i, "Doe", DEF))
        }
        assertEquals(21, sut.size)

        // Assert
        // This should throw
        sut.addPlayer(Player(22, "Doe", DEF))
    }

    @Test(expected = IllegalArgumentException::class)
    fun addPlayer_withDuplicateNumber_throwsIllegalArgumentException() {
        // Arrange
        val p1 = Player(3, "Doe", FWD)
        val p2 = Player(3, "Borg", DEF)
        val sut = Team().apply { addPlayer(p1) }

        // Act & Assert
        sut.addPlayer(p2)
    }

    @Test
    fun removePlayer_decrementsTheTeamCountByOne() {
        // Arrange
        val sut = Team().apply {
            addPlayer(playerA)
            addPlayer(playerB)
            addPlayer(playerC)
        }

        // Act
        sut.removePlayer(8)

        // Assert
        assertEquals(2, sut.size)
    }

    @Test
    fun removePlayer_whenPlayerDoesNotExist_doesNotChangeTeamCount() {
        // Arrange
        val sut = Team().apply {
            addPlayer(playerA)
            addPlayer(playerB)
            addPlayer(playerC)
        }

        // Act
        sut.removePlayer(123)

        // Assert
        assertEquals(3, sut.size)
    }

    @Test
    fun removePlayer_whenTeamIsEmpty_doesNotChangeTeamCount() {
        // Arrange
        val sut = Team()

        // Act
        sut.removePlayer(42)

        // Assert
        assertEquals(0, sut.size)
    }

    @Test
    fun canPlayGame_whenOneGoalkeeperAndTenPlayers_ReturnsTrue() {
        // Arrange
        val sut = Team()
        sut.addPlayer(Player(1, "Doe", GKP))
        for (i in 2..11) {
            sut.addPlayer(Player(i, "Doe", MDF))
        }

        // Act & Assert
        assertTrue(sut.canPlayGame())
    }

    @Test
    fun canPlayGame_whenOneGoalkeeperAndNinePlayers_ReturnsFalse() {
        // Arrange
        val sut = Team()
        sut.addPlayer(Player(1, "Doe", GKP))
        for (i in 2..10) {
            sut.addPlayer(Player(i, "Doe", MDF))
        }

        // Act & Assert
        assertFalse(sut.canPlayGame())
    }

    @Test
    fun canPlayGame_whenTwoGoalkeepersAndNinePlayers_ReturnsFalse() {
        // Arrange
        val sut = Team()
        sut.addPlayer(Player(1, "Doe", GKP))
        sut.addPlayer(Player(2, "Doe", GKP))
        for (i in 3..11) {
            sut.addPlayer(Player(i, "Doe", MDF))
        }

        // Act & Assert
        assertFalse(sut.canPlayGame())
    }

    @Test
    fun canPlayGame_whenNoGoalkeepersAndElevenPlayers_ReturnsFalse() {
        // Arrange
        val sut = Team()
        for (i in 1..11) {
            sut.addPlayer(Player(i, "Doe", MDF))
        }

        // Act & Assert
        assertFalse(sut.canPlayGame())
    }

    @Test
    fun canPlayGame_whenNoGoalkeepersAndNoPlayers_ReturnsFalse() {
        // Arrange
        val sut = Team()

        // Act & Assert
        assertFalse(sut.canPlayGame())
    }

    @Test
    fun savePlayers_whenStorageSuccess_ReturnsTrue() {
        // Arrange
        val sut = Team().apply { addPlayer(playerA) }

        // Act
        val result = sut.savePlayers(StubSaveSuccess())

        // Assert
        assertTrue(result)
    }

    @Test
    fun savePlayers_whenStorageFail_ReturnsFalse() {
        // Arrange
        val sut = Team().apply { addPlayer(playerA) }

        // Act
        val result = sut.savePlayers(StubSaveFail())

        // Assert
        assertFalse(result)
    }
}

class StubSaveSuccess : Storage {
    override fun save(player: Player) = 0
}

class StubSaveFail : Storage {
    override fun save(player: Player) = -1
}
