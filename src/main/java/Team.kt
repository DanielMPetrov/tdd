package ws2

import java.lang.IllegalArgumentException

interface Storage {
    fun save(player: Player): Int
}

data class Player(val number: Int, val surname: String, val role: Role)

enum class Role {
    GKP, DEF, MDF, FWD
}

class Team {

    companion object {
        const val MAX_PLAYERS = 21
    }

    private val players = ArrayList<Player>()

    private fun findPlayer(num: Int) = players.find { it.number == num }

    fun addPlayer(player: Player) {
        if (size >= MAX_PLAYERS) {
            throw IllegalStateException("Attempt to add a player in a full team")
        }
        if (players.any { it.number == player.number }) {
            throw IllegalArgumentException("Attempt to add a player with duplicate number")
        }
        players.add(player)
    }

    fun removePlayer(num: Int) {
        val player = findPlayer(num)
        players.remove(player)
    }

    fun canPlayGame() =
        players.any { it.role == Role.GKP } && players.count { it.role != Role.GKP } >= 10

    fun savePlayers(storage: Storage): Boolean {
        players.forEach {
            if (storage.save(it) == -1) {
                return false
            }
        }
        return true
    }

    val size: Int
        get() = players.size
}
